package com.mycompany.app;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class AppTest {
    @Test
    public void shouldNotFail() {
        List<String> someList = new ArrayList<>();
        someList.add("test");
        someList.stream().findFirst().orElseThrow();
        Assert.assertEquals(1, someList.size());
    }
}
